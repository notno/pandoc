FROM pandoc/latex

RUN apk add --no-cache make

RUN tlmgr update --self

RUN tlmgr install fvextra footnotebackref pagecolor mdframed zref needspace sourcesanspro sourcecodepro titling
